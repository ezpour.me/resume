from django import template

from blog.models import Hashtag, Category

register = template.Library()


@register.inclusion_tag("share/leftbar.html")
def leftbar():
    context = {
        "hashtags": Hashtag.objects.all()[:10]
    }
    return context


@register.inclusion_tag("share/header.html")
def header():
    context = {
        "categores": Category.objects.all()
    }
    return context
