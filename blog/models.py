import os
import random

from django.db import models
from django.utils import timezone

from extenctions.utils import jalali_converter


def get_filename_ext(filepath):  # get name snd ext from file path
    basename = os.path.basename(filepath)
    name, ext = os.path.splitext(basename)
    return name, ext


def set_image_path(instance, filename):
    new_name = random.randint(1, 23654512156786)
    name, ext = get_filename_ext(filename)
    final_name = f"{new_name}{ext}"
    return f"Post/{instance.title}/image/{final_name}"


def set_file_path(instance, filename):
    new_name = random.randint(1, 23654512156786)
    name, ext = get_filename_ext(filename)
    final_name = f"{new_name}{ext}"
    return f"Post/{instance.post.title}/file/{final_name}"


class Post(models.Model):
    STATUS_CHOICES = (
        ("d", "پیش نویس"),
        ("p", "منتشر شده")
    )
    title = models.CharField(verbose_name="عنوان", max_length=80, unique=True)
    slug = models.SlugField(verbose_name="اسلاگ", allow_unicode=True, max_length=80)
    body = models.TextField(verbose_name="متن", )
    publish = models.DateTimeField(verbose_name="تاریخ انتشار", default=timezone.now)
    create = models.DateTimeField(verbose_name="تاریخ به وجود آمدن", auto_now_add=True)
    update = models.DateTimeField(verbose_name="تاریخ اصلاح", auto_now=True)
    status = models.CharField(verbose_name="وضعیت", choices=STATUS_CHOICES, max_length=1)
    image = models.ImageField(verbose_name="تصویر", upload_to=set_image_path)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, verbose_name="دسته بندی",
                                 related_name='category_post',null=True)
    hashtag = models.ManyToManyField('Hashtag', verbose_name="برچسب(ها)",null=True)

    class Meta:
        verbose_name = "مطلب"
        verbose_name_plural = "مطالب"

    def __str__(self):
        return self.title

    def jpublish(self):
        return jalali_converter(self.publish)

    jpublish.short_description = "تاریخ انتشار"


class PostImage(models.Model):
    post = models.ForeignKey('Post', on_delete=models.CASCADE, related_name="Image_Post")
    image = models.ImageField(upload_to=set_image_path)


class PostFile(models.Model):
    post = models.ForeignKey('Post', on_delete=models.CASCADE, related_name="File_Post")
    file = models.ImageField(upload_to=set_file_path)


class Hashtag(models.Model):
    title = models.CharField(verbose_name="عنوان", max_length=50, unique=True)
    slug = models.SlugField(verbose_name="اسلاگ", allow_unicode=True)
    allow_to_show = models.BooleanField(verbose_name="آیا نمایش داده شود؟", default=False)

    class Meta:
        verbose_name = "برچسب"
        verbose_name_plural = "برچسب ها"

    def __str__(self):
        return self.title


class Category(models.Model):
    parent = models.ForeignKey('self', default=None, on_delete=models.SET_NULL, blank=True, related_name='children',
                               verbose_name="زیر دسته",null=True)
    name = models.CharField(max_length=30, verbose_name="عنوان", unique=True)
    slug = models.SlugField(allow_unicode=True)

    class Meta:
        verbose_name = "دسته بندی"
        verbose_name_plural = "دسته بندی ها"

    def __str__(self):
        return self.name

    def getLastPost(self):
        return self.category_post.all()[:4]
