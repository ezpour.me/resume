from django.contrib import admin
from django.utils.html import format_html

from . import models


# Register your models here.


class PostAdmin(admin.ModelAdmin):
    list_display = ("title","set_iamge" ,"set_body_lenght", "status", "jpublish", "category")
    list_filter = ("publish", "status")
    search_fields = ("title", "abstract", "body", "hashtag__title")
    prepopulated_fields = {"slug": ('title',)}
    ordering = ["-publish", "-create", "status"]
    actions = ["make_draft", "make_published"]

    def make_published(self, request, queryset):
        affect = queryset.update(status='p')
        self.message_user(request,
                          message="{} مورد با موفقیت منشر شد.".format(affect),
                          )

    make_published.short_description = "منتشر شود"

    def make_draft(self, request, queryset):
        affect = queryset.update(status='d')
        self.message_user(request,
                          message="{} مورد با موفقیت پیش نویس شد.".format(affect),
                          )

    make_draft.short_description = "پیش نویس شود"

    def set_body_lenght(self, obj):
        if len(obj.body) > 55:
            output = obj.body[0:55] + "..."
        else:
            output = obj.body
        return output

    set_body_lenght.short_description = "خلاصه"

    def set_iamge(self,obj):
        return format_html("<img with=100 height=75 style='border-radius:5px' src='{}'>".format(obj.image.url))


admin.site.register(models.Post, PostAdmin)


class Hashtagadmin(admin.ModelAdmin):
    list_display = ("title", "allow_to_show")
    list_filter = ("allow_to_show",)
    search_fields = ("title",)
    prepopulated_fields = {"slug": ('title',)}


admin.site.register(models.Hashtag, Hashtagadmin)


class Categoryadmin(admin.ModelAdmin):
    list_display = ("name", "parent")
    search_fields = ("name",)
    prepopulated_fields = {"slug": ('name',)}
    ordering = ["parent"]


admin.site.register(models.Category, Categoryadmin)
