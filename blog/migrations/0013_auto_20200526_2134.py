# Generated by Django 3.0.6 on 2020-05-26 17:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0012_auto_20200526_2133'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='title',
            field=models.CharField(max_length=80, unique=True, verbose_name='عنوان'),
        ),
    ]
