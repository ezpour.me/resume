from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404

from blog.models import Post, Category

context = {}


def home(request, page=1):
    allPost = Post.objects.filter(status="p").order_by("-publish")
    mainPost = Paginator(allPost, 5)
    context["lastPage"] = mainPost.num_pages
    context["postMain"] = mainPost.get_page(page)

    context["myPost"] = Category.objects.get(name__exact="دست نوشته")
    return render(request, "public/index.html",context)


def showpost(request,slug):
    context["post"] = get_object_or_404(Post,slug=slug)
    return render(request,"public/single.html",context)
