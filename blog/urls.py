from django.urls import path
from . import views

app_name = "blog"

urlpatterns = [
    path('', views.home, name="Home"),
    path('home/<int:page>', views.home, name="Home"),
    path('Post/<str:slug>', views.showpost, name="singlePost"),


]
