from django.utils import timezone

from . import jalali


def persion_convert_number(string):
    number = {
        "0": "۰",
        "1": "۱",
        "2": "۲",
        "3": "۳",
        "4": "۴",
        "5": "۵",
        "6": "۶",
        "7": "۷",
        "8": "۸",
        "9": "۹",
    }

    for e, p in number.items():
        string = string.replace(e, p)

    return string


def jalali_converter(time):
    month = ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"]
    time = timezone.localtime(time)

    time_to_str = "{},{},{}".format(time.year, time.month, time.day)
    time_to_jalali_tuple = jalali.Gregorian(time_to_str).persian_tuple()
    time_to_jalali_list = list(time_to_jalali_tuple)
    time_to_jalali_list[1] = month[time_to_jalali_list[1] - 1]

    output = "{} {} {} , ساعت {}:{}".format(
        time_to_jalali_list[2],
        time_to_jalali_list[1],
        time_to_jalali_list[0],
        time.hour,
        time.minute
    )
    return persion_convert_number(output)
